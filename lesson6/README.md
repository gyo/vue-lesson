# LESSON6

コンポーネントの分割

## 問題

* サインアップフォーム（有機体）の中身を、分子や原子に分割してください

## 作り方

1. `slot`(\*1)を使って分割してください
   1. ボタン、`fieldset`、`submit-block`を、`CButton.vue`, `CFieldset.vue`, `CFormSubmitBlock.vue`として分割してください。`slot`を使って、親コンポーネントから子コンポーネント内に表示する要素を渡せます
   1. 分割した SFC を使うには、`import`して`components`(\*2)に登録してください
1. パラメータが必要なものを、`props`(\*3)を使って分割してください
   1. フォーム、エラーメッセージを、`CForm.vue`, `CInlineMessage.vue`として分割してください。`:`を使って、分割したコンポーネントへパラメータを渡せます。渡されたパラメータは`props`で取得できます
   1. `<template></template>`ブロックでは、`data`, `computed`と同じように、`props`の値を利用できます
1. イベントが必要なものを、`$emit`(\*4)を使って分割してください
   1. テキストフォーム、チェックボックス、ラジオを、`CTextField.vue`, `CCheckbox.vue`, `CRadio.vue`として分割してください
   1. 親コンポーネントから渡されたメソッドは、`$emit`で実行できます

\*1 `slot`

https://jp.vuejs.org/v2/api/#slot

\*2 `components`

https://jp.vuejs.org/v2/api/#components

```
  components: {
  },
```

\*3 `props`

https://jp.vuejs.org/v2/api/#props

```
  props: {
  },
```

\*4 `$emit`

https://jp.vuejs.org/v2/api/#vm-emit

```
  this.$emit('hoge', arg1, arg2)
```
