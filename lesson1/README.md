# LESSON1

環境構築と、SFC(Single File Component)の作り方

## 問題

* Vue の SFC で「Hello World!」というテキストを表示してください

## 作り方

1. npm を使って必要なパッケージをインストールしてください
   1. Vue を使うために`vue`が必要です
   1. SFC を使うために`webpack`, `vue-template-compiler`, `vue-loader`, `css-loader`が必要です。lesson1 では、`css-loader`は無くてもビルド可能です
1. Webpack の設定ファイルを作ってください
   1. `webpack.config.js`を作ってください
   1. `entry.js`をビルドして`script.js`として出力するように設定を書いてください
   1. SFC を使うために、`.vue`ファイルには`vue-loader`を適用するような設定を書いてください
   1. おまじない(\*1)を書いてください
1. html に SFC を表示するための空 div を作ってください
   1. `index.html`に空`div`を作り、JS でアクセスするために ID `app`を振ってください
1. 表示する SFC を作ってください
   1. `App.vue`を作ってください
   1. `<template></template>`ブロック(\*2)を作ってください
   1. `<div>Hello World!</div>`と書いてください
1. SFC を html に表示するための js を作ってください
   1. `entry.js`に、`App.vue`を`#app`に表示する処理を書いてください。Vue インスタンスを作る(\*3)ことで表示できます

\*1 おまじない

```
  resolve: {
    alias: {
      vue: 'vue/dist/vue.esm.js',
    },
  }
```

\*2 `<template></template>`ブロック

```
<template>
</template>
```

\*3 Vue インスタンスを作る

https://jp.vuejs.org/v2/guide/instance.html

```
new Vue({

})
```
