# Vue.js 練習問題

Vue の練習問題です。Vue 未経験者が既存サイトを運用できるレベルになることを目標としています

各 lesson には、start, goal の 2 つのディレクトリがあります。start を編集し、goal の状態を目指してください

goal のファイルを確認しながら進めてかまいません

## 下準備

1. このリポジトリを clone してください
1. ローカルで起動するサーバーを用意してください。「Web Server for Chrome」がおすすめです

https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb

## lesson 一覧

1. 環境設定
1. イベントリスナ
1. データバインディング、条件分岐
1. フォームの実装、ループ
1. フォームの実装（エラー表示）
1. コンポーネントの分割
1. 非同期通信
1. ライフサイクル
