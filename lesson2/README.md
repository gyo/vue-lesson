# LESSON2

イベントリスナーのつけ方

## 問題

* 「Hello World!」というアラートを表示するボタンを設置してください

## 作り方

1. アラートを表示するメソッドを作ってください
   1. `<script></script>`ブロック(\*1)を作ってください
   1. `alert()`メソッドを作ってください。メソッドは`methods`(\*2)に定義します
1. メソッドをイベントにバインドしてください
   1. `@click="hoge()"`のように、`@`(\*3)を使ってメソッドをバインドできます

\*1 `<script></script>`ブロック

```
<script>
export default {
}
</script>
```

\*2 `methods`

https://jp.vuejs.org/v2/api/#methods

```
  methods: {
  },
```

\*3 `@`

https://jp.vuejs.org/v2/api/#v-on
